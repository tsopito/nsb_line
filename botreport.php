<?php
require "vendor/autoload.php";

$access_token = 'X5adYZqa1qLsZVYs5mXsI9SFe5HGhD3iMEXJT4Vqo4xaU7fUy/4UUmfxvranYWnE2xQEIy+Z4e4tT1hyc34PJIARlzKgGXV4YKoZaZegtDQadPDNg9WL66Ele4wR6YOdaxayZkzzGVUDY/iuso2w9wdB04t89/1O/w1cDnyilFU=';
$channelSecret = 'b158b4992cdb84aebf64dc123cec841c';

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "http://110.77.150.179/nsb/v2/json/report_json.php");
curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$json_data = json_decode(curl_exec($curl), true);
curl_close($curl);

for ($i = 0; $i < count($json_data); $i++) {
    $pushID = urldecode($json_data[$i]['token']);
    $message = urldecode($json_data[$i]['message']);
    $image_url1 = urldecode($json_data[$i]['image1']);
    $image_url2 = urldecode($json_data[$i]['image2']);
    $timeout = 0;
    $today = date("YmdHis");
	$image_file1 = 'botreport-' . $today . '-1.jpg';
	$image_file2 = 'botreport-' . $today . '-2.jpg';
	
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $image_url1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
    $image = curl_exec($ch);
    curl_close($ch);
    
    $handle = fopen($image_file1, 'w');
    fwrite($handle, $image);
    fclose($handle);
    
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $image_url2);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
    $image = curl_exec($ch);
    curl_close($ch);
    
    $handle = fopen($image_file2, 'w');
    fwrite($handle, $image);
    fclose($handle);
    
    $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($access_token);
	$bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $channelSecret]);
    
    $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($message);
    $response = $bot->pushMessage($pushID, $textMessageBuilder);
    
    echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
    
    $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder('https://did-line-bot.herokuapp.com/' . $image_file1, 'https://did-line-bot.herokuapp.com/' . $image_file1);
    $response = $bot->pushMessage($pushID, $textMessageBuilder);
    
    echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
	
    $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder('https://did-line-bot.herokuapp.com/' . $image_file2, 'https://did-line-bot.herokuapp.com/' . $image_file2);
    $response = $bot->pushMessage($pushID, $textMessageBuilder);
    
    echo $response->getHTTPStatus() . ' ' . $response->getRawBody();
}
