<?php
require "stickers.php";
require "vendor/autoload.php";
require_once('vendor/linecorp/line-bot-sdk/line-bot-sdk-tiny/LINEBotTiny.php');

$access_token = 'X5adYZqa1qLsZVYs5mXsI9SFe5HGhD3iMEXJT4Vqo4xaU7fUy/4UUmfxvranYWnE2xQEIy+Z4e4tT1hyc34PJIARlzKgGXV4YKoZaZegtDQadPDNg9WL66Ele4wR6YOdaxayZkzzGVUDY/iuso2w9wdB04t89/1O/w1cDnyilFU=';

$events = json_decode(file_get_contents('php://input'), true);

function str_begin($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function str_end($haystack, $needle)
{
    $length = strlen($needle);
    return $length === 0 ||
        (substr($haystack, -$length) === $needle);
}

function reply($access_token = '', $message = [])
{
    $url = 'https://api.line.me/v2/bot/message/reply';
    $post = json_encode($message);
    $headers = array('Content-Type: application/json', 'Authorization: Bearer ' . $access_token);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

if (!is_null($events['events'])) {
    foreach ($events['events'] as $event) {
        if ($event['type'] == 'message' and $event['source']['type'] == 'group' and ($event['message']['type'] == 'text' or $event['message']['type'] == 'sticker')) {
            $replyToken = $event['replyToken'];

            if ($event['message']['type'] == 'text') {
                $cmd = trim($event['message']['text']);

                if (str_begin($cmd, '!') or str_begin($cmd, '#')) {
                    if (str_begin($cmd, '!m')) {
                        $cmd = preg_replace("/[^0-9]/", "", $cmd);

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, "http://110.77.150.179/nsb/v2/json/mobile_json.php?value=" . $cmd);
                        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        $json_data = json_decode(curl_exec($curl), true);
                        curl_close($curl);

                        $text = '';

                        for ($i = 0; $i < count($json_data); $i++) {
                            if ($text != '') {
                                $text = $text . "\r\n\r\n";
                            }
                            $text = $text . 'เบอร์โทร : ' . $json_data[$i]['mobile_no']
                                . ' เลขบัตร : ' . $json_data[$i]['card_no']
                                . ' สถานะ : ' . $json_data[$i]['user_date'] . ' ' . $json_data[$i]['status'];
                        }

                        if ($text == '') {
                            $text = 'ไม่พบข้อมูล ' . $cmd;
                        }

                    } elseif (str_begin($cmd, '!p')) {
                        $cmd = preg_replace("/\!p/", "", $cmd);
                        $plate = preg_split("/[\s-]+/", trim($cmd));
                        $platenumber_count = 4 - strlen($plate[1]);
                        $plate[3] = $plate[1];

                        while ($platenumber_count > 0) {
                            $plate[3] = "0" . $plate[3];
                            $platenumber_count = $platenumber_count - 1;
                        }

                        $text = '';

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, "http://110.77.150.179/nsb/v2/json/plate_json.php?plate1=" . $plate[0] . "&plate2=" . $plate[3] . "&province=" . $plate[2]);
                        curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        $json_data = json_decode(curl_exec($curl), true);
                        curl_close($curl);


                        for ($i = 0; $i < count($json_data); $i++) {
                            if ($text != '') {
                                $text = $text . "\r\n\r\n";
                            }
                            $text = $text . "ประเภท : " . urldecode($json_data[$i]['ctype'])
                                . "\r\nยี่ห้อ : " . urldecode($json_data[$i]['brand'])
                                . "\r\nรุ่น : " . urldecode($json_data[$i]['model'])
                                . "\r\nสี่ : " . urldecode($json_data[$i]['color'])
                                . "\r\nผู้ครอบครอง : " . urldecode($json_data[$i]['name_occ'])
                                . "\r\nเจ้าของ : " . urldecode($json_data[$i]['name_own']);
                        }

                        if ($text == '') {
                            $text = 'ไม่พบข้อมูล ' . $plate[0] . ' ' . $plate[1] . ' ' . $plate[2];
                        }

                    } elseif (str_begin($cmd, '!lotto')) {
                        $text = " ";

                    } elseif (str_begin($cmd, '!id')) {
                        $text = "gID : " . $event['source']['groupId'];
                        //$text = "uID " . $event['source']['userID'];

                    } else {
                        $helps = ['ทำงี้ครับลูกพี่', 'เอาใหม่ๆ แบบนี้นะ', 'ไม่จำอ่ะ บอกว่างี้'];
                        $help = $helps[mt_rand(0, count($helps) - 1)];
                        $text = $help . "\r\n!m ตามด้วยเบอร์โทร หรือ เลขบัตร\r\n!p ตามด้วยทะเบียนรถ เว้นวรรคด้วยหล่ะ";
                    }

                    $messages = [
                        'type' => 'text',
                        'text' => $text
                    ];

                } else {
                    continue;
                }

            } elseif ($event['message']['type'] == 'sticker') {
                $sticker = $stickers[mt_rand(0, count($stickers) - 1)];

                $messages = [
                    'type' => 'sticker',
                    'packageId' => $sticker[1],
                    'stickerId' => $sticker[0]
                ];
            }

            if (isset($messages)) {
                $reply = [
                    'replyToken' => $replyToken,
                    'messages' => [$messages],
                ];

                $result = reply($access_token, $reply);
            }
        }
    }
}

echo 'OK';
